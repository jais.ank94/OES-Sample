import { OESAPPPage } from './app.po';

describe('oes-app App', () => {
  let page: OESAPPPage;

  beforeEach(() => {
    page = new OESAPPPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
