import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpModule} from '@angular/http';
import { RouterModule } from '@angular/router'; 
//import { BsDropdownModule} from 'ngx-bootstrap/dropdown';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LogindataService } from './logindata.service';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo/demo.component';
import { MaintestComponent } from './maintest/maintest.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    ProfileComponent,
    DashboardComponent,
    DemoComponent,
    MaintestComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
  //  BsDropdownModule.forRoot(),
    RouterModule.forRoot([
              {
                path: '',  component: LoginComponent
              },
              {
                path: 'dashboard', component: DashboardComponent
              },
              {
                path: 'profile', component:ProfileComponent
              },
              {
                path: 'maintest', component:MaintestComponent
              },
              {
                path: 'demo', component:DemoComponent
              }
              ])
  ],
  providers: [LogindataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
