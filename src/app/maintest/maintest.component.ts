import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-maintest',
  templateUrl: './maintest.component.html',
  styleUrls: ['./maintest.component.css']
})
export class MaintestComponent implements OnInit {

  form;

  private branches = [
    { id: 1, value: "Computer Science" },
    { id: 2, value: "Information Technology" },
    { id: 3, value: "Mechanical" },
    { id: 4, value: "Electronics" }
  ];
  private subjects = [];

  constructor(private router: Router) { }

  ngOnInit() {
     this.form= new FormGroup(
      {
        branch: new FormControl(),
        subject: new FormControl(),
      }
    );  
  }

  firstDropDownChanged(value: any) {
    const obj = this.branches[value];
    //console.log(value, obj);

    if (!obj) return;

    if (obj.id == 1) {
      this.subjects = ["Data Structure", "DBMS", "Web Technology"];
    }
    else if (obj.id == 2) {
      this.subjects = ["Applied Science", "Big DATA", "Animation Design"];
    }
    else if (obj.id == 3) {
      this.subjects = ["Thermo Dynamics", "Fluid Mechanics", "Hydrolics"];
    }
    else {
      this.subjects = [];
    }
  }

  onSubmit= function(login)
    {
     console.log(login); 
     
    }

}
