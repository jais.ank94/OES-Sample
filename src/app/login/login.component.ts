import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  title = 'OES Examination System';
  email:String;
  password:String;
  form;

  ngOnInit() {
    this.email='jais.ank94';
    this.password='hello';
    this.form= new FormGroup(
      {
        inputEmail: new FormControl(),
        inputPassword: new FormControl(),
      }
    ); 
  }

  onSubmit= function(login)
    {
     /*console.log(login);*/ 
     if(this.email==login.inputEmail && this.password==login.inputPassword )
     {
        this.router.navigateByUrl('/dashboard');
     }
     else{
       this.router.navigateByUrl('/');
     }
    }

}
